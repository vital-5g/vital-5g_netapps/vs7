#!/bin/bash

cd `dirname $0`/..

if [[ -f "$1" ]]; then
  echo "Using given '$1' as .env"
  cp "$1" .env
fi

if [[ ! -f .env ]]; then
  echo "Missing .env file, provide one as argument"
  exit 1
fi

. .env

docker build -t navigation-speed-optimizer:prod --target production-stage --build-arg HTTP_PORT=${HTTP_PORT} --build-arg QUEUE_PORT=${QUEUE_PORT} -f docker/Dockerfile ./
