#!/bin/sh

set -e

echo "Starting development server"
exec npm run serve
