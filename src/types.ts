export type NSO = {
  startIsrs: string|null,
  endIsrs: string|null,
  eta: string|null,
  routeLength: number,
  optimalSpeedKmh: number,
  optimalSpeedKnots: number,
  lastUpdateTimestamp: number|null,
};

export type VS5Msg = {
  latitude: number,
  longitude: number,
  eta: string,
};

export type VS8Msg = {
  latitude: number,
  longitude: number,
};
