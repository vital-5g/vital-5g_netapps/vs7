import axios from 'axios';

import { VS5Msg, VS8Msg } from './types';

exports.reverseGeocode = async (coordinates: VS5Msg|VS8Msg): Promise<string|null> => {
  const response = await axios.get(
    'https://vrisp4a.flaris.be/vrisgis/rest/services/Geocoding/LOC_RISIndex/GeocodeServer/reverseGeocode',
    {
      params: {
        location: `{"x":${coordinates.longitude},"y":${coordinates.latitude},"spatialReference":{"wkid":4326}}`,
        distance: 1000,
        f: 'json',
      },
    }
  ).catch(e => {
    console.error(e?.response?.data);
    return null;
  });

  if (response === null) {
    return null;
  }

  if (!response?.data?.address?.SingleKey) {
    console.error(`No reverse geocoding result: ${JSON.stringify(response.data)}`);
    return null;
  }

  return response.data.address.SingleKey;
};

exports.calculateRouteLength = async (startIsrs: string, endIsrs: string): Promise<number|null> => {
  const response: any = await axios.post(
    'https://www.visuris.be/voyagecalculatorapi/api/RouteCalculator/CalculateRoute',
    {
      stops: `${startIsrs};${endIsrs}`,
    }
  ).catch(e => {
    console.error(e?.response?.data);
    return null;
  });

  if (response === null) {
    return null;
  }

  if (!response?.data?.KortsteReis?.directions[0]?.summary) {
    console.error(`No route length result: ${JSON.stringify(response?.data)}`);
    return null;
  }

  return Math.round(response.data.KortsteReis.directions[0].summary.totalLength);
};
