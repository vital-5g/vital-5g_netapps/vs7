import http from 'http';
import express, { Application, Request, Response } from 'express';
import { Publisher, Subscriber } from 'zeromq';
const { calculateRouteLength, reverseGeocode } = require('./visuris');

import { VS5Msg, VS8Msg, NSO } from './types';

const app: Application = express();
app.use(express.json());
const server = http.createServer(app);

require('dotenv').config();

// environment variables check
['VS8_QUEUE_IP', 'VS8_QUEUE_PORT', 'QUEUE_PORT', 'HTTP_PORT'].forEach(envVar => {
  if (!process.env[envVar]) {
    console.error(envVar + ' environment variable not set.');
    process.exit(1);
  }
});

let nso: NSO = {
  startIsrs: null,
  endIsrs: null,
  eta: null,
  routeLength: 0,
  optimalSpeedKmh: 0,
  optimalSpeedKnots: 0,
  lastUpdateTimestamp: null,
};
let sockPub: Publisher|null = null;
const queueTopic = process.env['QUEUE_TOPIC'] || 'allData';

async function runNSOPublisher() {
  sockPub = new Publisher();

  sockPub.bind(`tcp://*:${process.env.QUEUE_PORT}`);
  console.log(`ZMQ publishing on *:${process.env.QUEUE_PORT}`);
}

async function runVS8Subscriber() {
  const VS8Sub = new Subscriber();

  VS8Sub.connect(`tcp://${process.env.VS8_QUEUE_IP}:${process.env.VS8_QUEUE_PORT}`);
  VS8Sub.subscribe(process.env['VS8_QUEUE_TOPIC'] || '');
  console.log(`Subscribed to VS8 on ${process.env.VS8_QUEUE_IP}:${process.env.VS8_QUEUE_PORT}`);

  for await (const msg of VS8Sub) {
    const VS8msg = parseVS8Msg(msg);
    if (VS8msg === null) {
      continue;
    }

    nso.startIsrs = await reverseGeocode(VS8msg);

    await calculateOptimalSpeed();

    if (sockPub !== null) {
      await sockPub.send(queueTopic.concat(JSON.stringify(nso)));
    }
  }
}

function parseVS8Msg(msg: Buffer[]): VS8Msg|null {
  let payload;

  try {
    payload = msg.toString();
    if (payload[0] === '"') {
      payload = JSON.parse(payload);
    }
    payload = JSON.parse(payload.substring(payload.indexOf("{")));
  } catch (e) {
    console.error(e);
    return null;
  }

  if (!payload?.geometry?.coordinates) {
    console.error(`Could not parse: ${msg.toString()}`);
    return null;
  }

  return { latitude: payload.geometry.coordinates[1], longitude: payload.geometry.coordinates[0] };
}

async function calculateOptimalSpeed() {
  if (nso.startIsrs === null || nso.endIsrs === null || nso.eta === null) {
    return;
  }

  nso.routeLength = await calculateRouteLength(nso.startIsrs, nso.endIsrs);
  if (nso.routeLength === null) {
    return;
  }

  const currentTimestamp = Date.now() / 1000;
  const etaDate = new Date(nso.eta);

  const etaTimestamp = etaDate.getTime() / 1000;
  const timeLeftInSeconds = etaTimestamp - currentTimestamp;

  nso.optimalSpeedKmh = nso.routeLength / timeLeftInSeconds * 3.6;
  nso.optimalSpeedKnots = Math.round(nso.optimalSpeedKmh * 0.54 * 10000) / 10000;
  nso.optimalSpeedKmh = Math.round(nso.optimalSpeedKmh * 10000) / 10000;
  nso.lastUpdateTimestamp = Math.round(currentTimestamp);
}

function runHttpServer() {
  server.listen(process.env.HTTP_PORT, () => {
    console.log('Http server listening on *:' + process.env.HTTP_PORT);
  });

  app.get('/', (req: Request, res: Response) => {
    res.json(nso);
  });

  app.post('/destination', async (req: Request, res: Response) => {
    if (!req.body?.latitude || !req.body?.longitude || !req.body?.desiredTimeOfArrivalUtc) {
      res.status(400);
      res.json({'error': 'invalid request'});
    } else {
      const VS5Msg: VS5Msg = {...req.body, eta: req.body.desiredTimeOfArrivalUtc, desiredTimeOfArrivalUtc: undefined};
      nso.endIsrs = await reverseGeocode(VS5Msg);
      nso.eta = VS5Msg.eta;
      await calculateOptimalSpeed();
      if (sockPub !== null) {
        await sockPub.send(queueTopic.concat(JSON.stringify(nso)));
      }
      res.json(nso);
    }
  });
}

runNSOPublisher();
runVS8Subscriber();
runHttpServer();
