# Navigation speed optimizer

This is the repository for Navigation speed optimizer service (VS7). It contains a Node.js server which sits between Onboard data collection and interfacing with vessel service (VS8) and Remote Vessel Monitoring service (VS5), communicating with them via ZMQ and rest API.

## Develop locally
* `npm install`
* `cp .env.example .env` or or use some other prefered nodejs way to set the environment variables
* `npm run serve`

## Or build and run development Docker image
* `scripts/build-dev.sh`
* `scripts/run-dev.sh`

## Then Build and run production Docker image
* `docker build -t VS7:prod -f docker/Dockerfile.prod ./`
* `cp .env.example .env`
* `docker run --env-file .env --rm -p 6060:6060/tcp -p 5555:5555/tcp VS7:prod`

## Reading the NSO object
* The docker container exposes a REST API on port 6060 and ZMQ queue on port 5555. You can read the NSO object via both and it should look like this:
```
export type NSO = {
  startIsrs: string|null,
  endIsrs: string|null,
  eta: string|null,
  routeLength: number,
  optimalSpeedKmh: number,
  optimalSpeedKnots: number,
  lastUpdateTimestamp: number|null,
};
```
* If you run the container via `scripts/run-{dev|prod}.sh` HTTP port is mapped to 6060 so you should be able to call the REST API like so:
```
$ curl -XGET http://localhost:6060 | jq

{
  "startIsrs": null,
  "endIsrs": null,
  "eta": null,
  "routeLength": null,
  "optimalSpeedKmh": 0,
  "optimalSpeedKnots": 0,
  "lastUpdateTimestamp": null
}
```
* Alternatively, ZMQ should be subscribe-able on port 5555 (default topic `allData`)

## Setting the destination
* Destination can be set either by POSTing it to the `/destination` REST endpoint
* Following message format is expected
```
{
  "latitude": 51.254887,
  "longitude": 4.231544,
  "desiredTimeOfArrivalUtc": "2012-07-09T19:22:09.1440844Z"
}
```

## Disclaimer
* NSO route calculation uses the VisuRIS API, which does not cover all routes in Belgium. You can see the coverage on the map at https://www.visuris.be/default.aspx?path=Diensten/Routeplanner. If the route calculation fails because start or end point is outside the covered region you'll still be able to read the NSO object, but it will not be updated and might look like the example above with initial values (route length and optimal speeds are not calculated).

## Logs
* The docker container logs to standard output
* In regards to the disclaimer above, you might see a message saying
```
{
  Message: 'Could not find a network node matching provided start point with isrs-code BEJPS04120W174800653\n'
}
```
